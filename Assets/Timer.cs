using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    public float timer, tempoStart;
    public TextMeshProUGUI timerText;

    bool isActive = true;
    bool isLose = false;
    // Start is called before the first frame update
    void Start()
    {
        timer = tempoStart;

    }

    // Update is called once per frame
    void Update()
    {
        if (!isActive) return;
       
        timer -= Time.deltaTime;

        int minuti = Mathf.FloorToInt(timer / 60f);
        int secondi = Mathf.FloorToInt(timer - minuti*60);

        timerText.text = string.Format("{0}:{1}", minuti.ToString("D2"), secondi.ToString("D2"));
        if (timer <= 0)
        {
            Lose();
        }
    }

    public void Lose()
    {
        isActive = false;
        timerText.gameObject.SetActive(false);
        Debug.Log("ho perso");

    }

    public void AddTime(float timeToAdd)
    {
        timer += timeToAdd;
        timer = Mathf.Clamp(timer, 0, tempoStart);
    }
}
