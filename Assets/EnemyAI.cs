using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
public enum States { idle, inseguimento, attacco, ricerca }

public class EnemyAI : MonoBehaviour
{
    public States statoAttuale;
    public AIPath path;
    public Transform player;
    public RaycastSpotlight vision;

    public Transform destinazione=null;
    public float rangeRandomSearch;
    // Start is called before the first frame update
    void Start()
    {
        statoAttuale = States.idle;
        nextTimeToReset = Random.Range(timeToRestartNewDest * 0.5f, timeToRestartNewDest * 1.5f);
    }

    // Update is called once per frame
    void Update()
    {
        switch (statoAttuale)
        {
            case (States.idle):
                Patrolling();

                if (CanEnemySeePlayer())
                {
                    statoAttuale = States.inseguimento;
                }

                break;
            case (States.inseguimento):
                Follow();
                if (IsEnemyLosePlayer())
                {
                    statoAttuale = States.ricerca;
                }
                if (IsPlayerInAttackRange())
                {
                    statoAttuale = States.attacco;
                }
                break;
            case (States.ricerca):
                Search();
                if (CanEnemySeePlayer())
                {
                    statoAttuale = States.inseguimento;
                }
                if (IsEnemyBoredToSearch())
                {
                    statoAttuale = States.idle;
                }

                break;
            case (States.attacco):
                Attack();
                statoAttuale = States.inseguimento;
                break;

        }
        Move();

    }

    public void Move()
    {
        if (destinazione!=null)
            path.destination = destinazione.position;

    }

    public Vector3 FindNewDestination(Vector3 origin)
    {
        Vector2 randomPoint = Random.insideUnitCircle;

        Vector3 randomPoint3d = origin + new Vector3(randomPoint.x, 0, randomPoint.y) * rangeRandomSearch;
        return randomPoint3d;
    }

    float timerPatrolling = 0;
    public float timeToRestartNewDest=10f;
    public float nextTimeToReset;
    public void Patrolling()
    {
        timerPatrolling += Time.deltaTime;
        if (timerPatrolling> nextTimeToReset)
        {
            nextTimeToReset = Random.Range(timeToRestartNewDest * 0.5f, timeToRestartNewDest * 1.5f);
            path.destination = FindNewDestination(this.transform.position);
            timerPatrolling = 0;
        }
      
    }

    float timerSearch = 0;
    Vector3 lastPlayerPosition;
    public void Search()
    {
        destinazione = null;

        timerSearch += Time.deltaTime;
        if (timerSearch > nextTimeToReset)
        {
            nextTimeToReset = Random.Range(timeToRestartNewDest * 0.2f, timeToRestartNewDest * 1f);
            path.destination = FindNewDestination(lastPlayerPosition);
            timerSearch = 0;
        }
    }
    public void Follow()
    {
        destinazione = player;
    }
    public void Attack()
    {
        //partire animazione
        //logica hit

        //finita animazione , rimette lo stato inseguimento
    }

    public bool CanEnemySeePlayer()
    {
        if (vision.detectTarget == null)
        {
            lastPlayerPosition = path.destination;
            return false;
        }
        else
            return true;
    }

    public bool IsEnemyLosePlayer()
    {
        if (vision.detectTarget == null)
            return true;

        else
            return false;
    }
    public bool IsPlayerInAttackRange()
    {
        Vector3 distanzaGiocatore = player.position - this.transform.position;
        float distance = distanzaGiocatore.magnitude;

        if (distance < 1)
            return true;

        return false;
    }

    float timerBored = 0;
    public float maxTimeSearch = 10f;
    public bool IsEnemyBoredToSearch()
    {
        timerBored += Time.deltaTime;
        if (timerBored > maxTimeSearch)
        {
            timerBored = 0;
            return true;
        }
        return false;
    }



}
